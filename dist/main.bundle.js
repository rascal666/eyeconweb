/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/style.scss");
/* harmony import */ var _scripts_hamburger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./scripts/hamburger */ "./src/scripts/hamburger.js");
/* harmony import */ var _scripts_hamburger__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_scripts_hamburger__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _scripts_setting_swiper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./scripts/setting-swiper */ "./src/scripts/setting-swiper.js");
/* harmony import */ var _assets_images_Logo_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./assets/images/Logo.png */ "./src/assets/images/Logo.png");
/* harmony import */ var _assets_images_obi_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./assets/images/obi.png */ "./src/assets/images/obi.png");
/* harmony import */ var _assets_images_leroy_png__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./assets/images/leroy.png */ "./src/assets/images/leroy.png");
/* harmony import */ var _assets_images_ficus_png__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./assets/images/ficus.png */ "./src/assets/images/ficus.png");
/* harmony import */ var _assets_images_pions_png__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./assets/images/pions.png */ "./src/assets/images/pions.png");
/* harmony import */ var _assets_images_star_png__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./assets/images/star.png */ "./src/assets/images/star.png");
/* harmony import */ var _assets_images_orchid_png__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./assets/images/orchid.png */ "./src/assets/images/orchid.png");
/* harmony import */ var _assets_images_aloe_png__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./assets/images/aloe.png */ "./src/assets/images/aloe.png");
/* harmony import */ var _assets_images_chrysanthemum_png__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./assets/images/chrysanthemum.png */ "./src/assets/images/chrysanthemum.png");
/* harmony import */ var _assets_images_plant_png__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./assets/images/plant.png */ "./src/assets/images/plant.png");
/* harmony import */ var _assets_images_soil_png__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./assets/images/soil.png */ "./src/assets/images/soil.png");
/* harmony import */ var _assets_images_wareting_png__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./assets/images/wareting.png */ "./src/assets/images/wareting.png");
/* harmony import */ var _assets_images_indoor_png__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./assets/images/indoor.png */ "./src/assets/images/indoor.png");
/* harmony import */ var _assets_images_clumbu_png__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./assets/images/clumbu.png */ "./src/assets/images/clumbu.png");
/* harmony import */ var _assets_images_buket_png__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./assets/images/buket.png */ "./src/assets/images/buket.png");
/* harmony import */ var _assets_images_views_png__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./assets/images/views.png */ "./src/assets/images/views.png");
/* harmony import */ var _assets_images_18_png__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./assets/images/18+.png */ "./src/assets/images/18+.png");
/* harmony import */ var _assets_images_user_png__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./assets/images/user.png */ "./src/assets/images/user.png");






















/***/ }),

/***/ "./src/scripts/hamburger.js":
/*!**********************************!*\
  !*** ./src/scripts/hamburger.js ***!
  \**********************************/
/***/ (() => {

// Плавный scroll
var hamburgerIcon = document.querySelector('.main_content');
var menuHamburger = document.querySelector('.menuHamburger');
var menuHamburgerNav = document.querySelector('.menuHamburger__nav');
var body = document.querySelector('.js-body');
hamburgerIcon.addEventListener('click', function (e) {
  if (e.target.classList.value == 'icon__bar') {
    if (menuHamburger.classList.contains('menuHamburger-block')) {
      menuHamburger.classList.remove('menuHamburger-block');
      body.style.overflow = 'auto';
    } else {
      menuHamburger.classList.add('menuHamburger-block');
      body.style.overflow = 'hidden';
    }
  }
});
menuHamburgerNav.addEventListener('click', function (e) {
  if (e.target.classList.value == 'nav__item') {
    menuHamburger.classList.remove('menuHamburger-block');
    body.style.overflow = 'auto';
  }
});

/***/ }),

/***/ "./src/scripts/setting-swiper.js":
/*!***************************************!*\
  !*** ./src/scripts/setting-swiper.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! swiper */ "./node_modules/swiper/swiper.esm.js");

var swiper = new swiper__WEBPACK_IMPORTED_MODULE_0__["default"](".slider-r", {
  direction: "horizontal",
  slidesPerView: 2,
  spaceBetween: 16,
  breakpoints: {
    320: {
      slidesPerView: 2.5
    }
  }
});
var swiperFlowers = new swiper__WEBPACK_IMPORTED_MODULE_0__["default"](".slider-flowers", {
  direction: "horizontal",
  slidesPerView: 1,
  spaceBetween: 16,
  breakpoints: {
    320: {
      slidesPerView: 1.2
    }
  }
});

/***/ }),

/***/ "./src/style.scss":
/*!************************!*\
  !*** ./src/style.scss ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./src/assets/images/18+.png":
/*!***********************************!*\
  !*** ./src/assets/images/18+.png ***!
  \***********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/18+.png";

/***/ }),

/***/ "./src/assets/images/Logo.png":
/*!************************************!*\
  !*** ./src/assets/images/Logo.png ***!
  \************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/Logo.png";

/***/ }),

/***/ "./src/assets/images/aloe.png":
/*!************************************!*\
  !*** ./src/assets/images/aloe.png ***!
  \************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/aloe.png";

/***/ }),

/***/ "./src/assets/images/buket.png":
/*!*************************************!*\
  !*** ./src/assets/images/buket.png ***!
  \*************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/buket.png";

/***/ }),

/***/ "./src/assets/images/chrysanthemum.png":
/*!*********************************************!*\
  !*** ./src/assets/images/chrysanthemum.png ***!
  \*********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/chrysanthemum.png";

/***/ }),

/***/ "./src/assets/images/clumbu.png":
/*!**************************************!*\
  !*** ./src/assets/images/clumbu.png ***!
  \**************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/clumbu.png";

/***/ }),

/***/ "./src/assets/images/ficus.png":
/*!*************************************!*\
  !*** ./src/assets/images/ficus.png ***!
  \*************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/ficus.png";

/***/ }),

/***/ "./src/assets/images/indoor.png":
/*!**************************************!*\
  !*** ./src/assets/images/indoor.png ***!
  \**************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/indoor.png";

/***/ }),

/***/ "./src/assets/images/leroy.png":
/*!*************************************!*\
  !*** ./src/assets/images/leroy.png ***!
  \*************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/leroy.png";

/***/ }),

/***/ "./src/assets/images/obi.png":
/*!***********************************!*\
  !*** ./src/assets/images/obi.png ***!
  \***********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/obi.png";

/***/ }),

/***/ "./src/assets/images/orchid.png":
/*!**************************************!*\
  !*** ./src/assets/images/orchid.png ***!
  \**************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/orchid.png";

/***/ }),

/***/ "./src/assets/images/pions.png":
/*!*************************************!*\
  !*** ./src/assets/images/pions.png ***!
  \*************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/pions.png";

/***/ }),

/***/ "./src/assets/images/plant.png":
/*!*************************************!*\
  !*** ./src/assets/images/plant.png ***!
  \*************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/plant.png";

/***/ }),

/***/ "./src/assets/images/soil.png":
/*!************************************!*\
  !*** ./src/assets/images/soil.png ***!
  \************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/soil.png";

/***/ }),

/***/ "./src/assets/images/star.png":
/*!************************************!*\
  !*** ./src/assets/images/star.png ***!
  \************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/star.png";

/***/ }),

/***/ "./src/assets/images/user.png":
/*!************************************!*\
  !*** ./src/assets/images/user.png ***!
  \************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/user.png";

/***/ }),

/***/ "./src/assets/images/views.png":
/*!*************************************!*\
  !*** ./src/assets/images/views.png ***!
  \*************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/views.png";

/***/ }),

/***/ "./src/assets/images/wareting.png":
/*!****************************************!*\
  !*** ./src/assets/images/wareting.png ***!
  \****************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "assets/images/wareting.png";

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	(() => {
/******/ 		var scriptUrl;
/******/ 		if (__webpack_require__.g.importScripts) scriptUrl = __webpack_require__.g.location + "";
/******/ 		var document = __webpack_require__.g.document;
/******/ 		if (!scriptUrl && document) {
/******/ 			if (document.currentScript)
/******/ 				scriptUrl = document.currentScript.src
/******/ 			if (!scriptUrl) {
/******/ 				var scripts = document.getElementsByTagName("script");
/******/ 				if(scripts.length) scriptUrl = scripts[scripts.length - 1].src
/******/ 			}
/******/ 		}
/******/ 		// When supporting browsers where an automatic publicPath is not supported you must specify an output.publicPath manually via configuration
/******/ 		// or pass an empty string ("") and set the __webpack_public_path__ variable from your code to use your own logic.
/******/ 		if (!scriptUrl) throw new Error("Automatic publicPath is not supported in this browser");
/******/ 		scriptUrl = scriptUrl.replace(/#.*$/, "").replace(/\?.*$/, "").replace(/\/[^\/]+$/, "/");
/******/ 		__webpack_require__.p = scriptUrl;
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"main": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkreact_start"] = self["webpackChunkreact_start"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendors-node_modules_swiper_swiper_esm_js"], () => (__webpack_require__("./src/index.js")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;
//# sourceMappingURL=main.bundle.js.map