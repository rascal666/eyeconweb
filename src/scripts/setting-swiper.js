import Swiper from 'swiper';


const swiper = new Swiper(".slider-r", {
    direction: "horizontal",
    slidesPerView: 2,
    spaceBetween: 16,
    breakpoints: {
        320: {
            slidesPerView: 2.5,
        },
    },
});

const swiperFlowers = new Swiper(".slider-flowers", {
    direction: "horizontal",
    slidesPerView: 1,
    spaceBetween: 16,
    breakpoints: {
        320: {
            slidesPerView: 1.2,
        },
    },
});


