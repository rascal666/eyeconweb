// Плавный scroll

const hamburgerIcon = document.querySelector('.main_content')
const menuHamburger = document.querySelector('.menuHamburger')
const menuHamburgerNav = document.querySelector('.menuHamburger__nav')
const body = document.querySelector('.js-body')

hamburgerIcon.addEventListener('click', e => {
    if (e.target.classList.value == 'icon__bar') {
        if (menuHamburger.classList.contains('menuHamburger-block')) {
            menuHamburger.classList.remove('menuHamburger-block')
            body.style.overflow = 'auto'
        } else {
            menuHamburger.classList.add('menuHamburger-block')
            body.style.overflow = 'hidden'
        }
    }
})

menuHamburgerNav.addEventListener('click', e => {
    if (e.target.classList.value == 'nav__item') {
        menuHamburger.classList.remove('menuHamburger-block')
        body.style.overflow = 'auto'
    }
})
