import "./style.scss"
import "./scripts/hamburger"
import "./scripts/setting-swiper"

import logoImg from './assets/images/Logo.png'
import obiImg from './assets/images/obi.png'
import leroyImg from './assets/images/leroy.png'
import ficusImg from './assets/images/ficus.png'
import pionsImg from './assets/images/pions.png'
import starImg from './assets/images/star.png'
import orchidImg from './assets/images/orchid.png'
import aloeImg from './assets/images/aloe.png'
import chrysanthemumImg from './assets/images/chrysanthemum.png'
import plantImg from './assets/images/plant.png'
import soilImg from './assets/images/soil.png'
import waretingImg from './assets/images/wareting.png'
import indoorImg from './assets/images/indoor.png'
import clumbuImg from './assets/images/clumbu.png'
import buketImg from './assets/images/buket.png'
import viewsImg from './assets/images/views.png'
import iconlimitation from './assets/images/18+.png'
import user from './assets/images/user.png'


